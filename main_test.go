package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_main(t *testing.T) {
	type args struct {
		method string
		url    string
		valid  bool
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "path / was exists",
			args: args{
				method: "GET",
				url:    "/",
				valid:  true,
			},
		},
		{
			name: "path /test was not exists",
			args: args{
				method: "GET",
				url:    "/test",
				valid:  false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request, err := http.NewRequest(tt.args.method, tt.args.url, nil)
			if err != nil {
				t.Fatal(err)
			}
			response := httptest.NewRecorder()
			handler := http.HandlerFunc(hand)
			handler.ServeHTTP(response, request)
			if tt.args.valid {
				if response.Code != http.StatusOK {
					t.Errorf(
						"Expected status %v, got %v",
						http.StatusOK,
						response.Code,
					)
				}
			} else {
				if response.Code != http.StatusNotFound {
					t.Errorf(
						"Expected status %v, got %v",
						http.StatusNotFound,
						response.Code,
					)
				}
			}
		})
	}
}
