package main

import (
	"fmt"
	"net/http"
	"os"

	_ "github.com/joho/godotenv/autoload"
)

var answ = os.Getenv("FILE_ENV")

func main() {
	if answ != "gitlab_env" {
		answ = "not env"
	}
	http.HandleFunc("/", hand)
	fmt.Println("Runned: http://127.0.0.1:8000")
	err := http.ListenAndServe("0.0.0.0:8000", nil)
	if err != nil {
		fmt.Println("Error:", err)
	}
}

func hand(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}
	fmt.Fprint(w, answ+" change1")
}
