FROM alpine:latest

COPY ./main /app/main

RUN chmod +x /app/main

ARG DOCKER_ENV

RUN echo "FILE_ENV=$DOCKER_ENV" > /app/.env

WORKDIR /app

CMD ["./main"]
